/*----------------------------------------------------------------------------------------------------------
::“Copyright 2018 Clayton Burnett”
::This program is distributed under the terms of the GNU General Public License
------------------------------------------------------------------------------------------------------------*/
/**
 * @fileOverview
 *
 * This file contains the main abstraction for a single screen
 *
 * @author Clayton Burnett <the82professional@hotmail.com>
 */
/**
 * ###############################################################################################################
 *                                              SMScreen
 */
/**
 * @class
 * Class modeling a canvas screen
 *
 * @description
 * A screen is modeled as a pair of blitting canvas elements
 **/
/**
* @constructor
*/
export class SMScreen {
    constructor(Canvas, IniRes, ID) {
        this.Canvas = Canvas;
        this.IniRes = IniRes;
        this.ID = ID;
        //Default Constructor
        this.Canvas = Canvas;
        this.IniRes = IniRes;
        this.ID = ID;
        this.Res = IniRes;
        this.DOMArray = new Array();
        this.ID = ID;
        this.MenuSpacing = 0; //Global Menu Spacing
        this.RenderFlag = true;
        //Text Images Menus Sprites Codeables Backgrounds
        this.RenderRate = 0; //Used internally to calculate refresh
        this.BackgroundImages = new Array(); //The Current array of background Images
        this.eooFlag = true;
        this.MenuSelection = 0;
        if (Canvas) {
            for (let i of Canvas) {
                if (this.eooFlag == true) {
                    this.ctx = i.getContext('2d');
                    this.eooFlag = false;
                }
                else {
                    this.bctx = i.getContext('2d');
                    this.eooFlag = true;
                }
            }
        }
    }
    //----------------------------------------------------------------SET METHODS---------------------------
    /**
     * Set the Menu Spacing
     * @param {number} Spacing Sets the menu spacing
     */
    set MenuElementSpacing(Spacing) {
        if (Spacing >= 0) {
            this.MenuSpacing = Spacing;
        }
        else {
            console.log("Cannot set the menu spacing to: " + Spacing);
        }
    }
    /**
    * Set the element screen resolution
    * @param {number[]} Resol Sets the screen resolution
    */
    set Resolut(Resol) {
        if (Resol && Resol.length > 0) {
            this.Res = Resol;
        }
        else {
            console.log("Cannot set the resolution: " + Resol);
        }
    }
    /**
    * Set the element screen resolution
    * @param {number} OP The menu item index
    */
    set MenuOption(OP) {
        if (OP >= 0) {
            this.MenuSelection = OP;
        }
        else {
            console.log("Cannot set the menu to: " + OP);
        }
    }
    /**
    * Set the text width
    * @param {number} Width Sets the text width to apply to created text elements
    */
    set SetTextWidth(Width) {
        if (Width >= 0) {
            this.TextWidth = Width;
        }
        else {
            console.log("Text width not defined in SetTextWidth(Try using an integer value)");
        }
    }
    /**
    * Set the line height
    * @param {number Height Sets the default line height to be applied to text elements
    */
    set SetLineHeight(Height) {
        if (Height >= 0) {
            this.LineHeight = Height;
        }
        else {
            console.log("Line height not defined in SetLineHeight(Try using an integer value)");
        }
    }
    /**
    * Set the element screen resolution
    * @param {number[]} Resolution Sets the resolution of the canvas element
    */
    set Resolution(Resolution) {
        if (Resolution.length == 2) {
            this.Res = Resolution;
        }
        else {
            console.log("Cannot set screen" + this.ID + "to resolution" + Resolution + "requires x/y integer array");
        }
    }
    /**
    * Sets the array of active DOMItems to process
    * @param {DOMItem[]} DOMArray Updates the screen DOM
    */
    set DOM(DOMArray) {
        if (DOMArray.length >= 0) {
            this.DOMArray = DOMArray;
        }
    }
    /**
    * Sets whether to render this screen during the screenmap render cycle
    * @param {boolean} Flag A boolean value indicating either true(Render) or false(do not render)
    */
    set Render(Flag) {
        if (Flag == true || false) {
            this.RenderFlag = Flag;
        }
        else {
            console.log("Render must be either true or false");
        }
    }
    //----------------------------------------------------------GET METHODS-------------------------------
    /**
    * Gets the unique screen ID
    * @returns {Number} The unique screen ID
    */
    get GetID() {
        return (Number(this.ID));
    }
    /**
    * Gets the current render count
    * @returns {Number} The current render count
    */
    get GetRenderSpeed() {
        return (Number(this.RenderRate));
    }
    /**
    * Gets the current render count
    * @returns {Boolean} The current render state(true:on, false:off)
    */
    get GetRenderState() {
        return (Boolean(this.RenderFlag));
    }
    /**
    * Gets the current canvas context
    * @returns {CanvasRenderingContext2D} The screen canvas context
    */
    get GetCTX() {
        return (this.ctx);
    }
    /**
   * Gets the current background canvas context
   * @returns {CanvasRenderingContext2D} The backgroud screen canvas context
   */
    get GetBCTX() {
        return (this.bctx);
    }
    //-----------------------------------------------------------PUBLIC INTERFACE---------------------------------
    /**
    * Funtion to add a Menu item to the current screen
    * @param {string[]} Items A compiled array of strings
    * @param {number[]} Origin The upper left box anchor coordinate
    * @param {number[]} Dimensions x/y array of dimensions
    * @param {string} Font The Menu Font String
    * @param {string} Style The Menu Style String
    * @param {string} Text An optional text description
    * @param {number} ID The unique identifier or menu handle
    */
    AddMenu(Items, Origin, Dimensions, Font, Style, Text, ID) {
        let Item = { dimensions: Dimensions, fillstyle: Style, font: Font, image: null, origin: Origin, type: "Menu", text: Text, id: ID, items: Items };
        this.DOMArray.push(Item);
    }
    /**
    * Function to add an item to the render DOM
    * @param {string[]} Items A compiled array of strings
    * @param {number[]} Origin Origin x/y of the upper left corner of the drawable
    * @param {number[]} Dimensions where to draw the sprite in an x/y number array
    * @param {string} Image Name of the sprite to use
    * @param {string} Type type of the sprite to use(Sprite|Text|Background)
    * @param {string} Font The font to apply
    * @param {string} Style The style to apply
    * @param {string} Text An optional text description
    * @param {number} ID The unique identifier or menu handle
    * @returns {number|string[]} A handle to the added object
    */
    Draw(Items, Origin, Dimensions, Image, Type, Font, FillStyle, Text, ID) {
        let Item = { dimensions: Dimensions, fillstyle: FillStyle, font: Font, image: Image, origin: Origin, type: Type, text: Text, id: ID, items: Items };
        this.DOMArray.push(Item);
        return (ID);
    }
    /**
    * Function to test whether the indicated point intersects with a DOM item and return the matches
    * @param {number[]} Point The point to test each item against
    * @param {number} Radius (optional)The Pixel radius centered at Point
    * @returns {string[]} An array of name strings for each item
    */
    ReturnIntersect(Point, Radius) {
        let Matches = new Array();
        if (Radius) {
            let PRad = Radius;
        }
        else {
            let PRad = 0;
        }
        for (let i of this.DOMArray) {
            let cond1 = (Point[0] >= i.origin[0] + i.dimensions[0]);
            let cond2 = (Point[0] + Radius <= i.origin[0]);
            let cond3 = (Point[1] >= i.origin[1] + i.dimensions[1]);
            let cond4 = (Point[1] + Radius <= i.origin[1]);
            if (!(cond1 || cond2 || cond3 || cond4)) {
                Matches.push(i.text);
            }
        }
        return (Matches);
    }
    /**
    * Function to render the map to the background canvas and blit
    */
    RenderDOM() {
        //Render Backgrounds First
        for (let i of this.DOMArray) {
            if (i.type == "Background") {
                //0=Image 1=Origin 2=Dimensions 3=Type
                this.bctx.drawImage(i.image, i.origin[0], i.origin[1], i.dimensions[0], i.dimensions[1]);
            }
        }
        //Render Sprites Next
        for (let i of this.DOMArray) {
            if (i.type == "Sprite") {
                //0=Image 1=Origin 2=Dimensions 3=Type
                this.bctx.drawImage(i.image, i.origin[0], i.origin[1], i.dimensions[0], i.dimensions[1]);
            }
        }
        //Render Text Last
        for (let i of this.DOMArray) {
            if (i.type == "Text") {
                this.bctx.fillStyle = i.fillstyle;
                this.bctx.font = i.font;
                this.ctx.fillStyle = i.fillstyle;
                this.ctx.font = i.font;
                this.WrapText(this.bctx, i.text, i.origin[0], i.origin[1], i.dimensions[0], i.dimensions[1]);
            }
        }
        //Render A Menu Last Last
        for (let i of this.DOMArray) {
            if (i.type == "Menu") {
                //Render the Menu given the current menu state
                this.bctx.fillStyle = i.fillstyle;
                this.bctx.font = i.font;
                this.ctx.fillStyle = i.fillstyle;
                this.ctx.font = i.font;
                //For each Menu element
                for (let j in i.items) {
                    if (this.MenuSelection === Number(j)) {
                        this.WrapText(this.bctx, "* " + i.items[j], i.origin[0], i.origin[1] + (this.MenuSpacing * Number(j)), i.dimensions[0] / i.dimensions[0], (i.dimensions[1] / i.items.length));
                    }
                    else {
                        this.WrapText(this.bctx, i.items[j], i.origin[0], i.origin[1] + (this.MenuSpacing * Number(j)), i.dimensions[0] / i.dimensions[0], (i.dimensions[1] / i.items.length));
                    }
                }
            }
        }
        //Refresh the screen
        this.Blit();
    }
    /**
    * Function to flip the displayed screens by swapping video buffer
    */
    Blit() {
        if (this.bctx && this.ctx) {
            let offscreen_data = this.bctx.getImageData(0, 0, this.Res[0], this.Res[1]);
            this.ctx.putImageData(offscreen_data, 0, 0);
        }
    }
    /**
    * Function to clear the DOM
    */
    ClearDOM() {
        this.DOMArray = new Array();
        this.Clear();
    }
    /**
    * Function to clear the DOM by ID
    */
    ClearID() {
        this.DOMArray = new Array();
        this.Clear();
    }
    //-----------------------------------------------------------INTERNAL METHODS---------------------------------------- 
    /**
    * Function to resize the canvas and zoom elements to fit the window maintains aspect ratio
    * @param {number[]} PageDimensions Array of XY corresponding to the pages native pixel dimensions
    * @param {number[]} WindowDimensions Array of XY corresponding to the current window dimensions
    */
    ScaleCanvas(PageDimensions, WindowDimensions) {
        //------------------------------------------------------FULL SCREEN MODE ONLY SO FAR-------------------------	
        //Save the canvas before performing transformation
        this.ctx.save();
        this.bctx.save();
        //if the upper left is 0,0
        let ratiox = (WindowDimensions[0] * 100) / PageDimensions[0]; //WindowDimensions[0] - PageDimensions[0];
        let ratioy = (WindowDimensions[1] * 100) / PageDimensions[1];
        ratioy = +ratioy.toFixed(2);
        ratiox = +ratiox.toFixed(2);
        ratioy = ratioy / 100;
        ratiox = 75.57 / 100;
        //if were not scaled correctly	
        if (ratiox != 100 || ratioy != 100) {
            this.ctx.scale(.50, .50);
            this.bctx.scale(.50, .50);
        }
        this.ctx.restore();
        this.bctx.restore();
        //--------------Implement a write text to screen function that can dynamically change the font position or size
    }
    /**
    * Clears the current screen (not needed with blitting)
    */
    Clear() {
        //--------------------------------------!!WARNING DEBUG MODE-------------------------------------------------------
        //Clear does not properly clear the defined resolution using the single command below
        //this.bctx.clearRect(0, 0, this.XResolution, this.YResolution);
        this.bctx.clearRect(0, 0, 3000, 3000);
    }
    /**
    * Function to wrap the loaded text
    * @param {CanvasRenderingContext2D} context the current Canvas context to display to
    * @param {string} text The text to display within the DOM element
    * @param {number} x The starting x position in pixels
    * @param {number} y The starting y position in pixels
    * @param {number} maxWidth The maximum width to use before wrap
    * @param {number} lineHeight The height of the display text
    */
    WrapText(context, text, x, y, maxWidth, lineHeight) {
        context.textAlign = "start";
        context.fillText(text, x, y);
    }
}
