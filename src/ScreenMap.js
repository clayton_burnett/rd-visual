/*----------------------------------------------------------------------------------------------------------
::“Copyright 2018 Clayton Burnett”
::This program is distributed under the terms of the GNU General Public License
------------------------------------------------------------------------------------------------------------*/
/**
 * @fileOverview
 *
 * This file contains the implementation for the Canvas controller
 *
 * @author Clayton Burnett <the82professional@hotmail.com>
 */
/**
 * ###############################################################################################################
 *                                              ScreenMap
 */
/**
 * @class
 * Class modeling a Multi-Canvas controller
 *
 * @description
 * This is the canvas manager. When passing in canvas DOM elements pass them in the order: front1, back1, front2, back2 etc.
 * The rear rear canvas element will be written to and then "blitted" to the front canvas.
 **/
import { SMScreen } from './SMScreen';
/**
* @constructor
*/
export class ScreenMap {
    constructor(CVChannels) {
        this.GlobalXResolution = 300; //Screen resolution X value in integer format
        this.GlobalYResolution = 150; //Screen resolution Y value in integer format
        this.GlobalStyle = "12px Arial";
        this.GlobalFont = 'blue';
        this.ZoomLevel = 1; //Current screen magnification level 
        this.WindowHeight = window.innerHeight;
        this.WindowWidth = window.innerWidth;
        this.Channels = CVChannels;
        this.Screens = [];
        this.GlobalRefreshLimit = 60;
        this.CycleCount = 0;
        this.IDIndex = 0;
        //Run on init function
        this.Init();
    }
    //----------------------------------------------SET METHODS--------------------------------------------------
    /**
    * Sets the default global canvas resolution for all managed canvases
    * @param {number[]} Pair An instance of a x[0] and y[1] integer array indicating a resolution
    */
    set SetGlobalResolution(Pair) {
        if (Pair.length > 1) {
            this.GlobalXResolution = Pair[0];
            this.GlobalYResolution = Pair[1];
            for (let i of this.Screens) {
                i.Resolut = [this.GlobalXResolution, this.GlobalYResolution];
            }
        }
    }
    /**
    * Sets the default global style applied to screen text elements
    * @param {string} Style A ctx.fillstyle string
    */
    set SetGlobalStyle(Style) {
        if (Style.length != 0) {
            this.GlobalStyle = Style;
        }
    }
    /**
    * Sets the default global font applied to screen text elements
    * @param {string} Font A ctx.font string
    */
    set SetGlobalFont(Font) {
        if (Font.length != 0) {
            this.GlobalFont = Font;
        }
    }
    //----------------------------------------------GET METHODS(NONE)-------------------------------------------------
    //----------------------------------------------PUBLIC INTERFACE--------------------------------------------------
    /**
    * Function to detect which element or elements exist at the coordinates indicated
    * @param {number} Screen The screen to detect on
    * @param {number[]} Coordinates The point to test each item against
    * @param {number} Radius (optional)The Pixel radius centered at Point
    * @returns {string[]} A string array of detected elements
    */
    ClickDetect(Screen, Coordinates, Radius) {
        if (Screen >= 0 && Coordinates.length == 2) {
            let TestScreen = this.Screens[Screen];
            if (Radius == undefined) {
                let Collisions = TestScreen.ReturnIntersect(Coordinates, 1);
                return (Collisions);
            }
            else {
                let Collisions = TestScreen.ReturnIntersect(Coordinates, Radius);
                return (Collisions);
            }
        }
    }
    /**
    * Function to calculate the width of text on a given screen
    * @param {number} Screen The screen number to analyze
    * @param {string} Text The Text to analyze
    * @returns {number[]} Number Array indicating the width and the height of the text respectively
    */
    CalculateTextWidth(Screen, Text) {
        if (Text.length != 0) {
            let refMheight = this.Screens[Screen].GetCTX.measureText("M").width;
            let width = this.Screens[Screen].GetCTX.measureText(Text).width;
            return [refMheight, width];
        }
        else {
            console.log("Cannot calculate the text width of an empty string");
        }
    }
    /**
    * @param {number} Screen The screen to address
    * @param {number} Option The Menu Option to set
    */
    MenuOption(Screen, Option) {
        if (Screen >= 0 && Option >= 0) {
            this.Screens[Screen].MenuOption = Option;
        }
        else {
            console.log("Cannot set menu option out of bounds value for Screen or Option");
        }
    }
    /**
    * @param {number} Screen The screen to address
    * @param {number} Spacing The menu spacing to set
    */
    MenuSpace(Screen, Spacing) {
        if (Screen >= 0 && Spacing >= 0) {
            this.Screens[Screen].MenuElementSpacing = Spacing;
        }
        else {
            console.log("Cannot set menu spacing");
        }
    }
    /**
    * Function to create a Menu
    * @param {number} Screen The screen the menu is associated with
    * @param {string[]} Items An array of the text string items to be included in the menu
    * @param {number[]} Origin The upper left box anchor coordinate
    * @param {number[]} Dimensions The Menu Dimensions
    * @param {number} ID The unique identifier for the menu object
    * @param {string} Text (optional) The optional text description of the menu
    * @param {string} Font (optional) The menu font, if not declared will use the current global settings
    * @param {string} Style (optional) The menu style, if not declared will use the current global settings
    */
    Menu(Screen, Items, Origin, Dimensions, ID, Text, Font, Style) {
        if (Screen >= 0 && Items.length > 0) {
            let fn, st;
            if (Font.length > 0) {
                fn = Font;
            }
            else {
                fn = this.GlobalFont;
            }
            if (Style.length > 0) {
                st = Style;
            }
            else {
                st = this.GlobalStyle;
            }
            this.Screens[Screen].AddMenu(Items, Origin, Dimensions, fn, st, Text, ID);
        }
        else {
            console.log("cannot add Menu Item (no Screen or Items Defined");
        }
    }
    /**
    * Function to write text to a screen
    * @param {number} Screen The canvas pair to render to
    * @param {string} Text The text to display
    * @param {number} x The starting x position in pixels
    * @param {number} y The starting y position in pixels
    * @param {number} Width The maximum width to use
    * @param {number} Height The height of the display text
    * @param {ImageBitmap} Pic (optional) picture representation
    * @returns {number} Handle to the written object
    */
    //Items: string[], Origin: number[], Dimensions: number[], Image: ImageBitmap, Type: string, Font: string, FillStyle: string, Text: string, ID: number
    WriteText(Screen, Text, xOrigin, yOrigin, Width, Height, Pic) {
        if (Screen >= 0) {
            //Origin, Dimensions, Image, Type, Font, FillStyle, Text
            let handle = this.Screens[Screen].Draw(["none"], new Array(xOrigin, yOrigin), new Array(Width, Height), Pic, "Text", this.GlobalFont, this.GlobalStyle, Text, this.IDIndex);
            this.IDIndex++;
            return (handle);
        }
    }
    /**
    * Function to write a background layer into the scene
    * @param {number} Screen The screen to render to
    * @param {number} xOrigin The starting x position in pixels
    * @param {number} yOrigin The starting y position in pixels
    * @param {number} Width The width of the background
    * @param {number} Height The height of the background
    * @param {ImageBitmap} Pic (optional) picture representation
    * @returns {number} Handle to the written object
    */
    WriteBackground(Screen, xOrigin, yOrigin, Width, Height, Pic) {
        if (Screen >= 0) {
            //Origin, Dimensions, Image, Type, Font, FillStyle, Text
            let handle = this.Screens[Screen].Draw(["none"], new Array(xOrigin, yOrigin), new Array(Width, Height), Pic, "Background", this.GlobalFont, this.GlobalStyle, "", this.IDIndex);
            this.IDIndex++;
            return (handle);
        }
    }
    /**
    * Function to write a background layer into the scene
    * @param {number} Screen The screen to render to
    * @param {number} xOrigin The starting x position in pixels
    * @param {number} yOrigin The starting y position in pixels
    * @param {number} Width The width of the Image
    * @param {number} Height The height of the Image
    * @param {ImageBitmap} Pic (optional) picture representation
    * @returns {number} Handle to the written object
    */
    WriteSprite(Screen, xOrigin, yOrigin, Width, Height, Pic) {
        if (Screen >= 0) {
            //Origin, Dimensions, Image, Type, Font, FillStyle, Text
            let handle = this.Screens[Screen].Draw(["none"], new Array(xOrigin, yOrigin), new Array(Width, Height), Pic, "Sprite", this.GlobalFont, this.GlobalStyle, "", this.IDIndex);
            this.IDIndex++;
            return (handle);
        }
    }
    /**
    * Function to write a background layer into the scene
    * @param {number} Screen The screen to clear
    */
    ClearScreen(Screen) {
        if (Screen >= 0) {
            //Origin, Dimensions, Image, Type, Font, FillStyle, Text
            this.Screens[Screen].ClearDOM();
        }
    }
    /**
    * Internal function that is run each render cycle
    */
    RenderCycle() {
        if (this.CycleCount < this.GlobalRefreshLimit) {
            this.CycleCount++; //Increment the cycle counter
            //Run screen updates
            for (let i of this.Screens) {
                if (i.GetRenderSpeed <= this.CycleCount && i.GetRenderState == true) {
                    i.RenderDOM();
                }
            }
        }
        else {
            //Reset the cycle counter
            this.CycleCount = 0;
        }
    }
    //----------------------------------------------PRIVATE MEMBER FUNCTIONS------------------------------------------
    /**
    * private ScreenMap initialization function, does not take parameters
    */
    Init() {
        //Create Screen objects for each pair
        let oddeven = 0;
        let ctxArray = new Array();
        let bctxArray = new Array();
        //Split up the channels
        for (let i of this.Channels) {
            if (oddeven == 0) {
                ctxArray.push(i);
                oddeven = 1;
            }
            else {
                bctxArray.push(i);
                oddeven = 0;
            }
        }
        for (let i in ctxArray) {
            this.Screens.push(new SMScreen([ctxArray[i], bctxArray[i]], new Array(this.GlobalXResolution, this.GlobalYResolution), Number(i)));
        }
    }
} //End Object
