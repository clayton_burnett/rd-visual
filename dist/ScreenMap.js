(function webpackUniversalModuleDefinition(root, factory) {
	if(typeof exports === 'object' && typeof module === 'object')
		module.exports = factory();
	else if(typeof define === 'function' && define.amd)
		define("ScreenMap", [], factory);
	else if(typeof exports === 'object')
		exports["ScreenMap"] = factory();
	else
		root["ScreenMap"] = factory();
})(window, function() {
return /******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./src/ScreenMap.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./src/SMScreen.js":
/*!*************************!*\
  !*** ./src/SMScreen.js ***!
  \*************************/
/*! exports provided: SMScreen */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SMScreen", function() { return SMScreen; });
/*----------------------------------------------------------------------------------------------------------
::“Copyright 2018 Clayton Burnett”
::This program is distributed under the terms of the GNU General Public License
------------------------------------------------------------------------------------------------------------*/
/**
 * @fileOverview
 *
 * This file contains the main abstraction for a single screen
 *
 * @author Clayton Burnett <the82professional@hotmail.com>
 */
/**
 * ###############################################################################################################
 *                                              SMScreen
 */
/**
 * @class
 * Class modeling a canvas screen
 *
 * @description
 * A screen is modeled as a pair of blitting canvas elements
 **/
/**
* @constructor
*/
class SMScreen {
    constructor(Canvas, IniRes, ID) {
        this.Canvas = Canvas;
        this.IniRes = IniRes;
        this.ID = ID;
        //Default Constructor
        this.Canvas = Canvas;
        this.IniRes = IniRes;
        this.ID = ID;
        this.Res = IniRes;
        this.DOMArray = new Array();
        this.ID = ID;
        this.MenuSpacing = 0; //Global Menu Spacing
        this.RenderFlag = true;
        //Text Images Menus Sprites Codeables Backgrounds
        this.RenderRate = 0; //Used internally to calculate refresh
        this.BackgroundImages = new Array(); //The Current array of background Images
        this.eooFlag = true;
        this.MenuSelection = 0;
        if (Canvas) {
            for (let i of Canvas) {
                if (this.eooFlag == true) {
                    this.ctx = i.getContext('2d');
                    this.eooFlag = false;
                }
                else {
                    this.bctx = i.getContext('2d');
                    this.eooFlag = true;
                }
            }
        }
    }
    //----------------------------------------------------------------SET METHODS---------------------------
    /**
     * Set the Menu Spacing
     * @param {number} Spacing Sets the menu spacing
     */
    set MenuElementSpacing(Spacing) {
        if (Spacing >= 0) {
            this.MenuSpacing = Spacing;
        }
        else {
            console.log("Cannot set the menu spacing to: " + Spacing);
        }
    }
    /**
    * Set the element screen resolution
    * @param {number[]} Resol Sets the screen resolution
    */
    set Resolut(Resol) {
        if (Resol && Resol.length > 0) {
            this.Res = Resol;
        }
        else {
            console.log("Cannot set the resolution: " + Resol);
        }
    }
    /**
    * Set the element screen resolution
    * @param {number} OP The menu item index
    */
    set MenuOption(OP) {
        if (OP >= 0) {
            this.MenuSelection = OP;
        }
        else {
            console.log("Cannot set the menu to: " + OP);
        }
    }
    /**
    * Set the text width
    * @param {number} Width Sets the text width to apply to created text elements
    */
    set SetTextWidth(Width) {
        if (Width >= 0) {
            this.TextWidth = Width;
        }
        else {
            console.log("Text width not defined in SetTextWidth(Try using an integer value)");
        }
    }
    /**
    * Set the line height
    * @param {number Height Sets the default line height to be applied to text elements
    */
    set SetLineHeight(Height) {
        if (Height >= 0) {
            this.LineHeight = Height;
        }
        else {
            console.log("Line height not defined in SetLineHeight(Try using an integer value)");
        }
    }
    /**
    * Set the element screen resolution
    * @param {number[]} Resolution Sets the resolution of the canvas element
    */
    set Resolution(Resolution) {
        if (Resolution.length == 2) {
            this.Res = Resolution;
        }
        else {
            console.log("Cannot set screen" + this.ID + "to resolution" + Resolution + "requires x/y integer array");
        }
    }
    /**
    * Sets the array of active DOMItems to process
    * @param {DOMItem[]} DOMArray Updates the screen DOM
    */
    set DOM(DOMArray) {
        if (DOMArray.length >= 0) {
            this.DOMArray = DOMArray;
        }
    }
    /**
    * Sets whether to render this screen during the screenmap render cycle
    * @param {boolean} Flag A boolean value indicating either true(Render) or false(do not render)
    */
    set Render(Flag) {
        if (Flag == true || false) {
            this.RenderFlag = Flag;
        }
        else {
            console.log("Render must be either true or false");
        }
    }
    //----------------------------------------------------------GET METHODS-------------------------------
    /**
    * Gets the unique screen ID
    * @returns {Number} The unique screen ID
    */
    get GetID() {
        return (Number(this.ID));
    }
    /**
    * Gets the current render count
    * @returns {Number} The current render count
    */
    get GetRenderSpeed() {
        return (Number(this.RenderRate));
    }
    /**
    * Gets the current render count
    * @returns {Boolean} The current render state(true:on, false:off)
    */
    get GetRenderState() {
        return (Boolean(this.RenderFlag));
    }
    /**
    * Gets the current canvas context
    * @returns {CanvasRenderingContext2D} The screen canvas context
    */
    get GetCTX() {
        return (this.ctx);
    }
    /**
   * Gets the current background canvas context
   * @returns {CanvasRenderingContext2D} The backgroud screen canvas context
   */
    get GetBCTX() {
        return (this.bctx);
    }
    //-----------------------------------------------------------PUBLIC INTERFACE---------------------------------
    /**
    * Funtion to add a Menu item to the current screen
    * @param {string[]} Items A compiled array of strings
    * @param {number[]} Origin The upper left box anchor coordinate
    * @param {number[]} Dimensions x/y array of dimensions
    * @param {string} Font The Menu Font String
    * @param {string} Style The Menu Style String
    * @param {string} Text An optional text description
    * @param {number} ID The unique identifier or menu handle
    */
    AddMenu(Items, Origin, Dimensions, Font, Style, Text, ID) {
        let Item = { dimensions: Dimensions, fillstyle: Style, font: Font, image: null, origin: Origin, type: "Menu", text: Text, id: ID, items: Items };
        this.DOMArray.push(Item);
    }
    /**
    * Function to add an item to the render DOM
    * @param {string[]} Items A compiled array of strings
    * @param {number[]} Origin Origin x/y of the upper left corner of the drawable
    * @param {number[]} Dimensions where to draw the sprite in an x/y number array
    * @param {string} Image Name of the sprite to use
    * @param {string} Type type of the sprite to use(Sprite|Text|Background)
    * @param {string} Font The font to apply
    * @param {string} Style The style to apply
    * @param {string} Text An optional text description
    * @param {number} ID The unique identifier or menu handle
    * @returns {number|string[]} A handle to the added object
    */
    Draw(Items, Origin, Dimensions, Image, Type, Font, FillStyle, Text, ID) {
        let Item = { dimensions: Dimensions, fillstyle: FillStyle, font: Font, image: Image, origin: Origin, type: Type, text: Text, id: ID, items: Items };
        this.DOMArray.push(Item);
        return (ID);
    }
    /**
    * Function to test whether the indicated point intersects with a DOM item and return the matches
    * @param {number[]} Point The point to test each item against
    * @param {number} Radius (optional)The Pixel radius centered at Point
    * @returns {string[]} An array of name strings for each item
    */
    ReturnIntersect(Point, Radius) {
        let Matches = new Array();
        if (Radius) {
            let PRad = Radius;
        }
        else {
            let PRad = 0;
        }
        for (let i of this.DOMArray) {
            let cond1 = (Point[0] >= i.origin[0] + i.dimensions[0]);
            let cond2 = (Point[0] + Radius <= i.origin[0]);
            let cond3 = (Point[1] >= i.origin[1] + i.dimensions[1]);
            let cond4 = (Point[1] + Radius <= i.origin[1]);
            if (!(cond1 || cond2 || cond3 || cond4)) {
                Matches.push(i.text);
            }
        }
        return (Matches);
    }
    /**
    * Function to render the map to the background canvas and blit
    */
    RenderDOM() {
        //Render Backgrounds First
        for (let i of this.DOMArray) {
            if (i.type == "Background") {
                //0=Image 1=Origin 2=Dimensions 3=Type
                this.bctx.drawImage(i.image, i.origin[0], i.origin[1], i.dimensions[0], i.dimensions[1]);
            }
        }
        //Render Sprites Next
        for (let i of this.DOMArray) {
            if (i.type == "Sprite") {
                //0=Image 1=Origin 2=Dimensions 3=Type
                this.bctx.drawImage(i.image, i.origin[0], i.origin[1], i.dimensions[0], i.dimensions[1]);
            }
        }
        //Render Text Last
        for (let i of this.DOMArray) {
            if (i.type == "Text") {
                this.bctx.fillStyle = i.fillstyle;
                this.bctx.font = i.font;
                this.ctx.fillStyle = i.fillstyle;
                this.ctx.font = i.font;
                this.WrapText(this.bctx, i.text, i.origin[0], i.origin[1], i.dimensions[0], i.dimensions[1]);
            }
        }
        //Render A Menu Last Last
        for (let i of this.DOMArray) {
            if (i.type == "Menu") {
                //Render the Menu given the current menu state
                this.bctx.fillStyle = i.fillstyle;
                this.bctx.font = i.font;
                this.ctx.fillStyle = i.fillstyle;
                this.ctx.font = i.font;
                //For each Menu element
                for (let j in i.items) {
                    if (this.MenuSelection === Number(j)) {
                        this.WrapText(this.bctx, "* " + i.items[j], i.origin[0], i.origin[1] + (this.MenuSpacing * Number(j)), i.dimensions[0] / i.dimensions[0], (i.dimensions[1] / i.items.length));
                    }
                    else {
                        this.WrapText(this.bctx, i.items[j], i.origin[0], i.origin[1] + (this.MenuSpacing * Number(j)), i.dimensions[0] / i.dimensions[0], (i.dimensions[1] / i.items.length));
                    }
                }
            }
        }
        //Refresh the screen
        this.Blit();
    }
    /**
    * Function to flip the displayed screens by swapping video buffer
    */
    Blit() {
        if (this.bctx && this.ctx) {
            let offscreen_data = this.bctx.getImageData(0, 0, this.Res[0], this.Res[1]);
            this.ctx.putImageData(offscreen_data, 0, 0);
        }
    }
    /**
    * Function to clear the DOM
    */
    ClearDOM() {
        this.DOMArray = new Array();
        this.Clear();
    }
    /**
    * Function to clear the DOM by ID
    */
    ClearID() {
        this.DOMArray = new Array();
        this.Clear();
    }
    //-----------------------------------------------------------INTERNAL METHODS---------------------------------------- 
    /**
    * Function to resize the canvas and zoom elements to fit the window maintains aspect ratio
    * @param {number[]} PageDimensions Array of XY corresponding to the pages native pixel dimensions
    * @param {number[]} WindowDimensions Array of XY corresponding to the current window dimensions
    */
    ScaleCanvas(PageDimensions, WindowDimensions) {
        //------------------------------------------------------FULL SCREEN MODE ONLY SO FAR-------------------------	
        //Save the canvas before performing transformation
        this.ctx.save();
        this.bctx.save();
        //if the upper left is 0,0
        let ratiox = (WindowDimensions[0] * 100) / PageDimensions[0]; //WindowDimensions[0] - PageDimensions[0];
        let ratioy = (WindowDimensions[1] * 100) / PageDimensions[1];
        ratioy = +ratioy.toFixed(2);
        ratiox = +ratiox.toFixed(2);
        ratioy = ratioy / 100;
        ratiox = 75.57 / 100;
        //if were not scaled correctly	
        if (ratiox != 100 || ratioy != 100) {
            this.ctx.scale(.50, .50);
            this.bctx.scale(.50, .50);
        }
        this.ctx.restore();
        this.bctx.restore();
        //--------------Implement a write text to screen function that can dynamically change the font position or size
    }
    /**
    * Clears the current screen (not needed with blitting)
    */
    Clear() {
        //--------------------------------------!!WARNING DEBUG MODE-------------------------------------------------------
        //Clear does not properly clear the defined resolution using the single command below
        //this.bctx.clearRect(0, 0, this.XResolution, this.YResolution);
        this.bctx.clearRect(0, 0, 3000, 3000);
    }
    /**
    * Function to wrap the loaded text
    * @param {CanvasRenderingContext2D} context the current Canvas context to display to
    * @param {string} text The text to display within the DOM element
    * @param {number} x The starting x position in pixels
    * @param {number} y The starting y position in pixels
    * @param {number} maxWidth The maximum width to use before wrap
    * @param {number} lineHeight The height of the display text
    */
    WrapText(context, text, x, y, maxWidth, lineHeight) {
        context.textAlign = "start";
        context.fillText(text, x, y);
    }
}


/***/ }),

/***/ "./src/ScreenMap.js":
/*!**************************!*\
  !*** ./src/ScreenMap.js ***!
  \**************************/
/*! exports provided: ScreenMap */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ScreenMap", function() { return ScreenMap; });
/* harmony import */ var _SMScreen__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./SMScreen */ "./src/SMScreen.js");
/*----------------------------------------------------------------------------------------------------------
::“Copyright 2018 Clayton Burnett”
::This program is distributed under the terms of the GNU General Public License
------------------------------------------------------------------------------------------------------------*/
/**
 * @fileOverview
 *
 * This file contains the implementation for the Canvas controller
 *
 * @author Clayton Burnett <the82professional@hotmail.com>
 */
/**
 * ###############################################################################################################
 *                                              ScreenMap
 */
/**
 * @class
 * Class modeling a Multi-Canvas controller
 *
 * @description
 * This is the canvas manager. When passing in canvas DOM elements pass them in the order: front1, back1, front2, back2 etc.
 * The rear rear canvas element will be written to and then "blitted" to the front canvas.
 **/

/**
* @constructor
*/
class ScreenMap {
    constructor(CVChannels) {
        this.GlobalXResolution = 300; //Screen resolution X value in integer format
        this.GlobalYResolution = 150; //Screen resolution Y value in integer format
        this.GlobalStyle = "12px Arial";
        this.GlobalFont = 'blue';
        this.ZoomLevel = 1; //Current screen magnification level 
        this.WindowHeight = window.innerHeight;
        this.WindowWidth = window.innerWidth;
        this.Channels = CVChannels;
        this.Screens = [];
        this.GlobalRefreshLimit = 60;
        this.CycleCount = 0;
        this.IDIndex = 0;
        //Run on init function
        this.Init();
    }
    //----------------------------------------------SET METHODS--------------------------------------------------
    /**
    * Sets the default global canvas resolution for all managed canvases
    * @param {number[]} Pair An instance of a x[0] and y[1] integer array indicating a resolution
    */
    set SetGlobalResolution(Pair) {
        if (Pair.length > 1) {
            this.GlobalXResolution = Pair[0];
            this.GlobalYResolution = Pair[1];
            for (let i of this.Screens) {
                i.Resolut = [this.GlobalXResolution, this.GlobalYResolution];
            }
        }
    }
    /**
    * Sets the default global style applied to screen text elements
    * @param {string} Style A ctx.fillstyle string
    */
    set SetGlobalStyle(Style) {
        if (Style.length != 0) {
            this.GlobalStyle = Style;
        }
    }
    /**
    * Sets the default global font applied to screen text elements
    * @param {string} Font A ctx.font string
    */
    set SetGlobalFont(Font) {
        if (Font.length != 0) {
            this.GlobalFont = Font;
        }
    }
    //----------------------------------------------GET METHODS(NONE)-------------------------------------------------
    //----------------------------------------------PUBLIC INTERFACE--------------------------------------------------
    /**
    * Function to detect which element or elements exist at the coordinates indicated
    * @param {number} Screen The screen to detect on
    * @param {number[]} Coordinates The point to test each item against
    * @param {number} Radius (optional)The Pixel radius centered at Point
    * @returns {string[]} A string array of detected elements
    */
    ClickDetect(Screen, Coordinates, Radius) {
        if (Screen >= 0 && Coordinates.length == 2) {
            let TestScreen = this.Screens[Screen];
            if (Radius == undefined) {
                let Collisions = TestScreen.ReturnIntersect(Coordinates, 1);
                return (Collisions);
            }
            else {
                let Collisions = TestScreen.ReturnIntersect(Coordinates, Radius);
                return (Collisions);
            }
        }
    }
    /**
    * Function to calculate the width of text on a given screen
    * @param {number} Screen The screen number to analyze
    * @param {string} Text The Text to analyze
    * @returns {number[]} Number Array indicating the width and the height of the text respectively
    */
    CalculateTextWidth(Screen, Text) {
        if (Text.length != 0) {
            let refMheight = this.Screens[Screen].GetCTX.measureText("M").width;
            let width = this.Screens[Screen].GetCTX.measureText(Text).width;
            return [refMheight, width];
        }
        else {
            console.log("Cannot calculate the text width of an empty string");
        }
    }
    /**
    * @param {number} Screen The screen to address
    * @param {number} Option The Menu Option to set
    */
    MenuOption(Screen, Option) {
        if (Screen >= 0 && Option >= 0) {
            this.Screens[Screen].MenuOption = Option;
        }
        else {
            console.log("Cannot set menu option out of bounds value for Screen or Option");
        }
    }
    /**
    * @param {number} Screen The screen to address
    * @param {number} Spacing The menu spacing to set
    */
    MenuSpace(Screen, Spacing) {
        if (Screen >= 0 && Spacing >= 0) {
            this.Screens[Screen].MenuElementSpacing = Spacing;
        }
        else {
            console.log("Cannot set menu spacing");
        }
    }
    /**
    * Function to create a Menu
    * @param {number} Screen The screen the menu is associated with
    * @param {string[]} Items An array of the text string items to be included in the menu
    * @param {number[]} Origin The upper left box anchor coordinate
    * @param {number[]} Dimensions The Menu Dimensions
    * @param {number} ID The unique identifier for the menu object
    * @param {string} Text (optional) The optional text description of the menu
    * @param {string} Font (optional) The menu font, if not declared will use the current global settings
    * @param {string} Style (optional) The menu style, if not declared will use the current global settings
    */
    Menu(Screen, Items, Origin, Dimensions, ID, Text, Font, Style) {
        if (Screen >= 0 && Items.length > 0) {
            let fn, st;
            if (Font.length > 0) {
                fn = Font;
            }
            else {
                fn = this.GlobalFont;
            }
            if (Style.length > 0) {
                st = Style;
            }
            else {
                st = this.GlobalStyle;
            }
            this.Screens[Screen].AddMenu(Items, Origin, Dimensions, fn, st, Text, ID);
        }
        else {
            console.log("cannot add Menu Item (no Screen or Items Defined");
        }
    }
    /**
    * Function to write text to a screen
    * @param {number} Screen The canvas pair to render to
    * @param {string} Text The text to display
    * @param {number} x The starting x position in pixels
    * @param {number} y The starting y position in pixels
    * @param {number} Width The maximum width to use
    * @param {number} Height The height of the display text
    * @param {ImageBitmap} Pic (optional) picture representation
    * @returns {number} Handle to the written object
    */
    //Items: string[], Origin: number[], Dimensions: number[], Image: ImageBitmap, Type: string, Font: string, FillStyle: string, Text: string, ID: number
    WriteText(Screen, Text, xOrigin, yOrigin, Width, Height, Pic) {
        if (Screen >= 0) {
            //Origin, Dimensions, Image, Type, Font, FillStyle, Text
            let handle = this.Screens[Screen].Draw(["none"], new Array(xOrigin, yOrigin), new Array(Width, Height), Pic, "Text", this.GlobalFont, this.GlobalStyle, Text, this.IDIndex);
            this.IDIndex++;
            return (handle);
        }
    }
    /**
    * Function to write a background layer into the scene
    * @param {number} Screen The screen to render to
    * @param {number} xOrigin The starting x position in pixels
    * @param {number} yOrigin The starting y position in pixels
    * @param {number} Width The width of the background
    * @param {number} Height The height of the background
    * @param {ImageBitmap} Pic (optional) picture representation
    * @returns {number} Handle to the written object
    */
    WriteBackground(Screen, xOrigin, yOrigin, Width, Height, Pic) {
        if (Screen >= 0) {
            //Origin, Dimensions, Image, Type, Font, FillStyle, Text
            let handle = this.Screens[Screen].Draw(["none"], new Array(xOrigin, yOrigin), new Array(Width, Height), Pic, "Background", this.GlobalFont, this.GlobalStyle, "", this.IDIndex);
            this.IDIndex++;
            return (handle);
        }
    }
    /**
    * Function to write a background layer into the scene
    * @param {number} Screen The screen to render to
    * @param {number} xOrigin The starting x position in pixels
    * @param {number} yOrigin The starting y position in pixels
    * @param {number} Width The width of the Image
    * @param {number} Height The height of the Image
    * @param {ImageBitmap} Pic (optional) picture representation
    * @returns {number} Handle to the written object
    */
    WriteSprite(Screen, xOrigin, yOrigin, Width, Height, Pic) {
        if (Screen >= 0) {
            //Origin, Dimensions, Image, Type, Font, FillStyle, Text
            let handle = this.Screens[Screen].Draw(["none"], new Array(xOrigin, yOrigin), new Array(Width, Height), Pic, "Sprite", this.GlobalFont, this.GlobalStyle, "", this.IDIndex);
            this.IDIndex++;
            return (handle);
        }
    }
    /**
    * Function to write a background layer into the scene
    * @param {number} Screen The screen to clear
    */
    ClearScreen(Screen) {
        if (Screen >= 0) {
            //Origin, Dimensions, Image, Type, Font, FillStyle, Text
            this.Screens[Screen].ClearDOM();
        }
    }
    /**
    * Internal function that is run each render cycle
    */
    RenderCycle() {
        if (this.CycleCount < this.GlobalRefreshLimit) {
            this.CycleCount++; //Increment the cycle counter
            //Run screen updates
            for (let i of this.Screens) {
                if (i.GetRenderSpeed <= this.CycleCount && i.GetRenderState == true) {
                    i.RenderDOM();
                }
            }
        }
        else {
            //Reset the cycle counter
            this.CycleCount = 0;
        }
    }
    //----------------------------------------------PRIVATE MEMBER FUNCTIONS------------------------------------------
    /**
    * private ScreenMap initialization function, does not take parameters
    */
    Init() {
        //Create Screen objects for each pair
        let oddeven = 0;
        let ctxArray = new Array();
        let bctxArray = new Array();
        //Split up the channels
        for (let i of this.Channels) {
            if (oddeven == 0) {
                ctxArray.push(i);
                oddeven = 1;
            }
            else {
                bctxArray.push(i);
                oddeven = 0;
            }
        }
        for (let i in ctxArray) {
            this.Screens.push(new _SMScreen__WEBPACK_IMPORTED_MODULE_0__["SMScreen"]([ctxArray[i], bctxArray[i]], new Array(this.GlobalXResolution, this.GlobalYResolution), Number(i)));
        }
    }
} //End Object


/***/ })

/******/ });
});
//# sourceMappingURL=ScreenMap.js.map