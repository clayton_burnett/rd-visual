/*-------------------------------------------------------------------
::"Copywright 2019 Clayton Burnett"
::This program is distributed under the terms of the GNU General Public License
--------------------------------------------------------------------*/

/**
 * @fileoverview
 * 
 * This is the main game file for Luft
 * 
 * @author Clayton Burnett <the82professional@hotmail.com>
 */

 /**
  * ##########################################################################
  *                                 Luft
  */
 /**
  * @class
  * Class modeling the main game Engine
  * 
  * @description
  * !!!IMPORTANT!!! The main engine for Luft requires Access to the RD-Visual and RD-Audio Libraries
  **/
 /**
  * @constructor
  */

  //The Main runtime
  var Luft = function(){
    //Game Global Variables(infrequently used)
    this.GameStart = false;
    //this.counter = 1;
	  this.Loaded = 0; //Reload Flag
    this.Stage = 0;
    let SCLib = window.ScreenMap;
		let SCMap = new Array();
    //--------------------------------------FIND A HOME FOR ME ----------------------------------
    //Function Called to switch the stage
	  Luft.prototype.Init = function(){

        this.container = document.createElement("DIV");
				this.Front1 = document.createElement("canvas");
				this.Back1 = document.createElement("canvas");
				document.body.appendChild(this.container);
				this.container.appendChild(this.Front1);
				this.container.appendChild(this.Back1);
				this.container.setAttribute('class', 'container');
				this.Front1.setAttribute('class', 'view f1');
				this.Front1.setAttribute('id', 'Front1');
				this.Front1.innerText = "Your browser does not support the HTML5 canvas element";
				this.Back1.setAttribute('class', 'view b1');
				this.Back1.setAttribute('id', 'Back1');
        this.Back1.innerText = "Your browser does not support the HTML5 canvas element";
        let Channels = new Array();
        Channels.push(this.Front1);
				Channels.push(this.Back1);
				this.SCMap = new SCLib.ScreenMap(Channels);
        this.SCMap.SetGlobalResolution = [500, 500];
        this.SCMap.SetGlobalStyle = 'red';
        this.SCMap.SetGlobalFont = "20px Arial";
        this.SCMap.MenuSpace(0, 70);
        this.Front1.width = 500;
				this.Front1.height = 500;
				this.Back1.width = 500;
				this.Back1.height = 500;
				this.counter = 1;
				this.GameStart = false;
				this.Loaded = 0; //Reload Flag
				this.Stage = 0;
				//Player Stat Config
				this.PPos = new Array(100, 300);
				this.PAngle = Math.PI / 2;
				this.PMoveAngle = 0;
				this.PThrust = 0;
				this.PXSpeed = 0;
				this.PYSpeed = 0;
				this.PlayerEntity = new Array(0, this.PPos[0], this.PPos[1], 150, 150, document.getElementById("4"));
				//Entity Type Setup
				this.Entities = new Array();
				this.Entities.push(this.PlayerEntity);
				this.MenuSelection = 0;
        this.MenuLevel = 0;
        this.fontsize = 25;
        this.color = "red";
        this.MenuActive = 1;

    }
    Luft.prototype.CalcPositions = function(){
      this.PAngle += this.PMoveAngle * Math.PI / 180;
      this.PPos[0] += this.PThrust * Math.sin(this.PAngle);
      this.PPos[1] -= this.PThrust * Math.cos(this.PAngle);
      this.PlayerEntity[1] = this.PPos[0];
      this.PlayerEntity[2] = this.PPos[1];
      //Reset the Angle modifier
      this.PMoveAngle = 0;
      //Check Collisions
    }
    Luft.prototype.UpdateCycle = function(){
      this.CalcPositions();
      this.SCMap.RenderCycle();
      let ctx = document.getElementById('Back1').getContext('2d');
      ctx.save();
      ctx.translate(this.PPos[0] + this.PlayerEntity[3], this.PPos[1] + this.PlayerEntity[4]);
      ctx.rotate(this.PAngle);
      //ctx.fillStyle = color;
      //ctx.fillRect(this.width / -2, this.height / -2, this.width, this.height);
      ctx.restore();    
    }
    //Function Called to switch the stage
	  Luft.prototype.SwitchStage = function(){

    }
    //Runtime prototype takes a run mode eg.difficulty and returns an exitstate hash		
    Luft.prototype.Run = function(Mode){

    }

    //Private function to generate the state hash
    Luft.prototype.GenerateSaveState = function(){
        
    }
    //Display menu function
    Luft.prototype.MenuDisplay = function(){
      let MenuXY = [180, 230];
      let MenuDim = [50, 50];
      this.SCMap.MenuSpace(0, 50);
      let fontsz = 35;
      let fontstl = "px sans";

      switch(this.MenuLevel){
        case 0:
          this.SCMap.Menu(0, ["Start Game", "Options"], MenuXY, MenuDim, 1, "Menu Level 1",fontsz + fontstl, this.color);
        break;
        case 1:
          this.SCMap.Menu(0, ["Game Resolution", "Audio", "Back"], MenuXY, MenuDim, 1, "Menu Level 2",fontsz + fontstl, this.color);
        break
        default:
        break;
      }
      this.SCMap.MenuOption(0, this.MenuSelection);
    }
    Luft.prototype.MenuDo = function(){
      switch(this.MenuLevel){
        case 0:
          switch(this.MenuSelection){
            case 0:
              this.Stage = 1;
              this.GameStart = true;
              this.MenuActive = 0;
            break;
            case 1:
              this.MenuLevel = 1;
            break;
            default:
            break;
          }
        break;
        case 1:
          switch(this.MenuSelection){
            case 0:
            //Game Res
              alert("Res Changed");
            break;
            case 1:
              alert("Audio Changed");
            //Audio
            break;
            case 2:
              this.MenuLevel = 0;
              this.MenuSelection = 1;
            //Back
            break;
            default:
            break;
          }
        break;
        default:
        break;
      }
    }
  }
  //Object for menu Management
  var Menu = function(){
    //Global Menu Variable
    this.MenuSelection = 0;
	  this.MenuLevel = 0;

    //------MENU SCREENS-------------------------

  }
  //Object for Entity Management
  var Entity = function(){
      //Global Entity Variables
      //Entity Type Setup
		  this.Entities = new Array();
	    this.Entities.push(this.PlayerEntity);
      //Player Stat Config
      this.PThrust = 2;
      this.PXSpeed = 0;
      this.PYSpeed = 0;
      this.PPos = new Array(50, 100);
      this.PAngle = Math.PI / 2;
		  this.PMoveAngle = 0;
      this.PlayerEntity = new Array(0, this.PPos[0], this.PPos[1], 150, 150, document.getElementById("4"));

  }
  //Entity AI definitions
  var AI = function(){
    //Global AI Variables
    

  }
  //Stage setup definitions
  var Stage = function(){
    //There should be no global Stage Variables as the definitions are read only
    Stage.prototype.MainMenu = function(){

    }
    Stage.prototype.Level = function(){

    }
  }