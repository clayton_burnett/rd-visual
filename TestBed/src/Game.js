/*-------------------------------------------------------------------
::"Copywright 2019 Clayton Burnett"
::This program is distributed under the terms of the GNU General Public License
--------------------------------------------------------------------*/

/**
 * @fileoverview
 * 
 * This is the main game file for Luft
 * 
 * @author Clayton Burnett <the82professional@hotmail.com>
 */

 /**
  * ##########################################################################
  *                                 Luft
  */
 /**
  * @class
  * Class modeling the main game Engine
  * 
  * @description
  * !!!IMPORTANT!!! The main engine for Luft requires Access to the RD-Visual and RD-Audio Libraries
  **/
 /**
  * @constructor
  */

  //The Main runtime
  var Luft = function(){
    //Game Global Variables(infrequently used)
    this.GameStart = false;
    //this.counter = 1;
	  this.Loaded = 0; //Reload Flag
    this.Stage = 0;
    let SCLib = window.ScreenMap;
		let SCMap = new Array();
    //--------------------------------------FIND A HOME FOR ME ----------------------------------
    //Function Called to switch the stage
	  Luft.prototype.Init = function(){
        let Channels = new Array();
				Channels.push(document.getElementById("Front1"));
				Channels.push(document.getElementById("Back1"));

				this.SCMap = new SCLib.ScreenMap(Channels);
				this.SCMap.SetGlobalResolution = [1500, 700];
				this.counter = 1;
				this.GameStart = false;
				this.Loaded = 0; //Reload Flag
				this.Stage = 0;
				//Player Stat Config
				this.PPos = new Array(300, 300);
				this.PAngle = Math.PI / 2;
				this.PMoveAngle = 0;
				this.PThrust = 2;
				this.PXSpeed = 0;
				this.PYSpeed = 0;
				this.PlayerEntity = new Array(0, this.PPos[0], this.PPos[1], 150, 150, document.getElementById("4"));
				//Entity Type Setup
				this.Entities = new Array();
				this.Entities.push(this.PlayerEntity);
				this.MenuSelection = 0;
				this.MenuLevel = 0;

    }
    Luft.prototype.CalcPositions = function(){
      this.PAngle += this.PMoveAngle * Math.PI / 180;
      this.PPos[0] += this.PThrust * Math.sin(this.PAngle);
      this.PPos[1] -= this.PThrust * Math.cos(this.PAngle);
      this.PlayerEntity[1] = this.PPos[0];
      this.PlayerEntity[2] = this.PPos[1];
      //Reset the Angle modifier
      this.PMoveAngle = 0;
      //Check Collisions
    }
    Luft.prototype.UpdateCycle = function(){
      CalcPositions();
      this.SCMap.RenderCycle();
      let ctx = document.getElementById('Back1').getContext('2d');
      ctx.save();
      ctx.translate(this.PPos[0] + this.PlayerEntity[3], this.PPos[1] + this.PlayerEntity[4]);
      ctx.rotate(this.PAngle);
      //ctx.fillStyle = color;
      //ctx.fillRect(this.width / -2, this.height / -2, this.width, this.height);
      ctx.restore();    
    }
    //Function Called to switch the stage
	  Luft.prototype.SwitchStage = function(){

    }
    //Runtime prototype takes a run mode eg.difficulty and returns an exitstate hash		
    Luft.prototype.Run = function(Mode){

    }

    //Private function to generate the state hash
    Luft.prototype.GenerateSaveState = function(){
        
    }			
  }
  //Object for menu Management
  var Menu = function(){
    //Global Menu Variable
    this.MenuSelection = 0;
	  this.MenuLevel = 0;

    //------MENU SCREENS-------------------------

  }
  //Object for Entity Management
  var Entity = function(){
      //Global Entity Variables
      //Entity Type Setup
		  this.Entities = new Array();
	    this.Entities.push(this.PlayerEntity);
      //Player Stat Config
      this.PThrust = 2;
      this.PXSpeed = 0;
      this.PYSpeed = 0;
      this.PPos = new Array(300, 300);
      this.PAngle = Math.PI / 2;
		  this.PMoveAngle = 0;
      this.PlayerEntity = new Array(0, this.PPos[0], this.PPos[1], 150, 150, document.getElementById("4"));

  }
  //Entity AI definitions
  var AI = function(){
    //Global AI Variables
    

  }
  //Stage setup definitions
  var Stage = function(){
    //There should be no global Stage Variables as the definitions are read only
    Stage.prototype.MainMenu = function(){

    }
    Stage.prototype.Level = function(){

    }
  }