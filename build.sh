#!/bin/sh

#Script to build from typescript
tsc -target ES6 src/TSsrc/ScreenMap.ts src/TSsrc/SMScreen.ts
echo "Typescript Built"
cp src/TSsrc/ScreenMap.js src/TSsrc/SMScreen.js src
echo "copied"
npx webpack
echo Webpack run
cp dist/* TestBed
echo "copied to testbed"
